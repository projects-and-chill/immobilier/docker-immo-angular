# Application immobilière (Angular version)

## FAST TRIAL ENVIRONMENT

:warning: Prerequis nécéssaires :
- Avoir `Docker`
- Avoir la commande `make` (execution de **makefile**), cette commande est installée par defaut sur les OS linux tel que Ubuntu, Fedora, ... )

### :fast_forward:  Lancer l'application:

1. Lancer l'app localement  ( Clone services from git repo & execute docker-compose command)

```cmd
make up demo
```


2. Supprimer l'app et nettoyer ses volumes, images et networks

```cmd
make clear demo
```

### :calling: In app

Se connecter en tant qu'admin :
* mail : `admin@gmail.com`
* password : `admin`



## DEV ENVIRONMENT

1. Récupérer les services et lancer l'environement

```cmd
make init 
```

```cmd
make up dev #not yet implemented
```

2. Supprimer les containers

```cmd
make down dev #not yet implemented
```

3. Supprimer l'app et nettoyer ses volumes, images et networks

```cmd
make clear dev #not yet implemented
```


## :wrench: CONFIGURATION PAR DEFAUT :
* PORT par défaut : `80`
* HOTE par défaut : `localhost`

Les configurations peuvent être changées dans le fichier :  `.env`


## LES TECHNOS

L'application à été réalisée avec

- Nodejs
- Angular
- Nginx
- Mongodb
- Scss

* L'application a été stylisée .
* L'application possède 90% de ses fonctionnalités, certaines pages n'ont pas été créées


### Les 4 services :
- [Front angular](https://gitlab.com/projects-and-chill/immobilier/angular-front-immobilier)
- [Api rest nodejs](https://gitlab.com/projects-and-chill/immobilier/nodejs-api-immobilier) 
- [Reverse proxy nginx](https://gitlab.com/models8/nginx-revserse-proxy)
- Base de données mongodb




